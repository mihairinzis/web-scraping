import puppeteer, { Browser } from "puppeteer";
import * as cheerio from 'cheerio';
import { sentimentService } from "./sentiment-service";
import { Item } from "./item";
import { HttpError } from "./error";

export class ScraperService {
  private browser: Promise<Browser> | null = null;

  public getPageData(url: string): Promise<Item[] | string[]> {
    try {
      new URL(url);
    } catch (e) {
      throw new HttpError('Invalid URL', 400);
    }

    if (!url.startsWith(process.env.TEST_URL || 'https://wsa-test.vercel.app')) {
      return this.getPageHtml(url)
        .then(html => this.extractImages(html, url));
    }

    return this.getPageHtml(url)
      .then(html => this.extractWsaTestItems(html, url))
      .then(items => this.buildWsaTestSentiments(items));
  }

  private async getPageHtml(url: string): Promise<string> {
    const page = await this.getBrowser()
      .then(browser => browser.newPage());

    return page.goto(url, { waitUntil: 'networkidle0' })
      .then(() => page.content())
      .then(html => {
        page.close();
        return html;
      });
  }

  private async getBrowser(): Promise<Browser> {
    if (!this.browser) {
      this.browser = puppeteer.launch({ headless: "new" });
    }
    return this.browser;
  }

  private extractImages(html: string, url: string): string[] {
    const $ = cheerio.load(html);
    const urlObj = new URL(url);
    const urlRoot = `${urlObj.protocol}//${urlObj.hostname}`;

    return $('img').toArray()
      .map(el => $(el).attr('src') as string)
      .filter(src => !!src)
      .map(link => link.startsWith('http') ? link : urlRoot + link);
  }

  private extractWsaTestItems(html: string, url: string): Item[] {
    const $ = cheerio.load(html);
    return $('div a img').toArray()
      .map(el => el?.parent?.parent)
      .map(item => ({
        time: $('time', item).text(),
        img: url + $('img', item).attr('src')?.slice(1),
        href: url + $('a', item).attr('href')?.slice(1),
        title: $('a span', item).parent().text(),
        short_description: $('a span', item).parent().parent().next().text()
      }));
  }

  private buildWsaTestSentiments(items: Item[]): Promise<Item[]> {
    return Promise.all(
      items.map(item => this.getPageHtml(item.href)
        .then(html => cheerio.load(html))
        .then($ => $('div div div div div div div').text())
      )
    ).then(htmls => htmls.map((html, i) => {
      const words = html.split(' ').length;
      const sentiment = sentimentService.getSentiment(html);
      return { ...items[i], words, sentiment };
    }));
  }
}

export const scraperService = new ScraperService()
