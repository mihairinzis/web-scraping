import 'express-async-errors';
import { NextFunction, Request, Response, Router } from 'express';
import { scraperService } from './scraper-service';

const router = Router();

router.get('/scrap', async (req: Request, res: Response, next: NextFunction) => {
  const url = req.query.url as string;

  res.status(200).json(
    await scraperService.getPageData(url)
  );
});

router.get('*', function (req: Request, res: Response, next: NextFunction) {
  const err = new Error('Page not found');
  (err as any).statusCode = 404;
  next(err);
});

router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
  console.error(err.stack);
  res.status((err as any).statusCode || 500).send(err.message);
});

export default router;
