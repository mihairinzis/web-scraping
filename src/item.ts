export interface Item {
  time: string,
  img: string,
  href: string,
  title: string,
  short_description: string,
  words?: number,
  sentiment?: 'positive' | 'negative' | 'neutral'
}
