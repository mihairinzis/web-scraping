export class SentimentService {

  public getSentiment(...phrases: string[]): 'positive' | 'negative' | 'neutral' {
    const totalWeight = phrases
      .flatMap(phrase => phrase.split(' '))
      .reduce((acc, curr) => acc + this.getWeight(curr), 0);

    if (totalWeight > 1)
      return 'positive';
    if (totalWeight < -1)
      return 'negative';
    return 'neutral';
  }

  private getWeight(word: string): number {
    if (this.positive.find(pos => word.toLowerCase().includes(pos)))
      return 1
    if (this.negative.find(neg => word.toLowerCase().includes(neg)))
      return -1;
    return 0;
  }

  private positive = ['joy', 'positive', 'quiet', 'candid', 'coping', 'green', 'calm', 'joyful', 'good'];
  private negative = ['disappointing', 'junk', 'critical', 'fast-food', 'negative', 'bad', 'pollution', 'noise'];
}

export const sentimentService = new SentimentService()
