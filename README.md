# Web Scraping Project

This project is a web scraping application built with Node.js and TypeScript. It allows you to scrape data from websites using Puppeteer and Cheerio. There's also a simple one-file client interface where you can trigger the scraping actions.

## Installation

Before running the project, make sure you have Node.js (v18 was used for developement) and npm installed. Clone the repository and install the dependencies using the following commands:

```bash
git clone https://gitlab.com/mihairinzis/web-scraping.git
cd web-scraping
npm install
```

### Building & running

The project provides several npm scripts to simplify development and deployment processes:

- **Build TypeScript files:**
  ```bash
  npm run build
  ```

- **Start development server with automatic TypeScript compilation:**
  ```bash
  npm run dev
  ```

- **Build TypeScript files and start the production server:**
  ```bash
  npm run prod
  ```

### How to use

1. **Start the client**: *Open the `index.html` file from the root of the project.
1. **Enter URL**: Type the URL you want to scrape into the input field.
2. **Scrap Data**: Click the "Scrap" button to initiate the scraping process.
3. **View Results**:
    - If the URL is not from the specified test domain (https://wsa-test.vercel.app/), the scraped content is displayed as a grid of images.
    - If the URL is from the specified test domain, the scraped data is presented as formatted JSON.

## Node server details

### Simple express API

* **Scraping Endpoint (/scrap)**: This endpoint is responsible for handling GET requests to the `/scrap` route. It expects a query parameter `url` representing the URL to be scraped. When a request is received, it delegates the scraping task to the `scraperService.getPageData(url)` function. The response is sent back as JSON data.

### Technologies

- [Cheerio](https://github.com/cheeriojs/cheerio): Fast, flexible, and lean implementation of core jQuery for the server.
- [Cors](https://github.com/expressjs/cors): Middleware for enabling Cross-Origin Resource Sharing (CORS) in Express.js applications.
- [Dotenv](https://github.com/motdotla/dotenv): Zero-dependency module that loads environment variables from a .env file.
- [Express](https://expressjs.com/): Fast, unopinionated, minimalist web framework for Node.js.
- [Express-Async-Errors](https://www.npmjs.com/package/express-async-errors): Middleware to handle asynchronous route handlers in Express.js.
- [Puppeteer](https://github.com/puppeteer/puppeteer): Headless Chrome Node.js API for browser automation.

## Html client details

### One file to rule them all

By keeping all the HTML, CSS, JavaScript, and external dependencies within a single file, the codebase remains highly accessible and easy to manage. There's no need to navigate between multiple files, reducing complexity.

Although I'm comfortable with heavy frontend technologies such as Angular, I always wanted to work on projects where getting things done is more important than getting things 'corporate'.

### Technologies Used

- **Alpine.js**: A minimal JavaScript framework used for interactivity and data binding, enabling dynamic content updates without complex configurations.
- **Pico CSS**: A lightweight CSS framework used for minimal styling, ensuring a clean and modern appearance.

Certainly! Web scraping can be a powerful tool for gathering data from websites. Depending on your application's requirements, here are some additional scraping features that you might consider adding:

## Ideas for improvement

* Pagination Support
* Login and Session Management
* Captcha Handling
