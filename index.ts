import http from 'http';
import express, { Application } from 'express';
import router from './src/routes';
import cors from 'cors';
import 'dotenv/config';

const app: Application = express();
const port = process.env.PORT || 3000;
export const server = http.createServer(app);

app.use(cors());
app.use(router);

process.on('uncaughtException', (reason: Error | any) => {
  console.log(`Uncaught Exception: ${reason.message || reason}`);
});

process.on('unhandledRejection', (reason: Error | any) => {
  console.log(`Unhandled Rejection: ${reason.message || reason}`);
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
